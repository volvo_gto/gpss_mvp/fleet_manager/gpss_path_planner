use r2r::ParameterValue;
use std::collections::HashMap;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct ControllerParameters {
    pub transporters: Vec<String>,
    pub seconds_before_blocked: f64
}

/// Initialize parameters given a set of ros parameters
pub fn init_parameters(ros_params: HashMap<String, ParameterValue>) -> ControllerParameters {
    ControllerParameters {
        transporters: ros_params.get("transporters").and_then(get_string_array).unwrap_or(vec!(
            "atr_16".to_string(), 
            "atr_10".to_string(),
            "atr_12".to_string()
        )),
        seconds_before_blocked: ros_params.get("seconds_before_blocked").and_then(get_f64).unwrap_or(30.0),
    }
}

/// Given the old state, a new parameter, and a new value, return a new parameters object if
/// it has changed
pub fn update_parameter(parameters: ControllerParameters,
                        param_name: &str, param_val: ParameterValue) -> Option<ControllerParameters> {

    // no change.
    println!("Not changing parameter {}", param_name);
    return None;
}

fn get_i16(ros_param: &ParameterValue) -> Option<i16> {
    if let r2r::ParameterValue::Integer(i) = ros_param {
        Some((*i).try_into().expect("overflow atr_id"))
    } else {
        None
    }
}
fn get_f64(ros_param: &ParameterValue) -> Option<f64> {
    if let r2r::ParameterValue::Double(f) = ros_param {
        Some(*f)
    } else {
        None
    }

}
fn get_bool(ros_param: &ParameterValue) -> Option<bool> {
    if let r2r::ParameterValue::Bool(b) = ros_param {
        Some(*b)
    } else {
        None
    }
}
fn get_string_array(ros_param: &ParameterValue) -> Option<Vec<String>> {
    if let r2r::ParameterValue::StringArray(xs) = ros_param {
        Some(xs.clone())
    } else {
        None
    }
}
