use futures::{Stream, StreamExt};
use r2r::builtin_interfaces::msg::Duration;
use r2r::scene_manipulation_msgs::msg::TFExtra;

use r2r::QosProfile;
use std::collections::HashMap;
use std::error::Error;
use std::sync::{Arc, Mutex};

use r2r::geometry_msgs::msg::{Point, Pose, Quaternion, Transform};
use r2r::std_msgs::msg::{ColorRGBA, Header};
use r2r::{builtin_interfaces::msg::Time, geometry_msgs::msg::Vector3};
use serde::{Deserialize, Serialize};
use std::collections::HashSet;

pub static NODE_ID: &'static str = "visualization_server";
pub static BUFFER_MAINTAIN_RATE: u64 = 100;
pub static MARKER_PUBLISH_RATE: u64 = 20;
pub static FRAME_LIFETIME: i32 = 3; //seconds

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct FrameData {
    // mandatory fields in the json files
    pub parent_frame_id: String, // the id of the frame's parent frame
    pub child_frame_id: String,  // the id of the frame
    pub transform: Transform,    // where is the child frame defined in the parent
    // optional fields in the json files. will be encoded to a json string before sent out
    pub extra_data: ExtraData,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct ExtraData {
    pub time_stamp: Option<Time>, // the idea is that all frames should have this, but some don't
    pub zone: Option<f64>,        // when are you "at" the frame, threshold, in meters
    pub next: Option<HashSet<String>>, // this list can be used to store data for planners and visualizers
    pub frame_type: Option<String>, // can be used to distinguish if a frame is a waypoint, tag, human, etc.
    pub active: Option<bool>, // only active frames are manipulatable. undefined will be added as active
    pub show_mesh: Option<bool>, // if the frame should also visualize something or not
    pub mesh_type: Option<i32>, // 1 - cube, 2 - sphere, 3 - cylinder or 10 - mesh (provide path)
    pub mesh_path: Option<String>, // where to find the mesh path if mesh_type was 10
    pub mesh_scale: Option<f32>, // not all meshes are in mm values
    pub mesh_r: Option<f32>,
    pub mesh_g: Option<f32>,
    pub mesh_b: Option<f32>,
    pub mesh_a: Option<f32>,
}

impl Default for ExtraData {
    fn default() -> Self {
        ExtraData {
            time_stamp: None,
            zone: None,
            next: None,
            frame_type: None,
            active: None,
            show_mesh: None,
            mesh_type: None,
            mesh_path: None,
            mesh_scale: None,
            mesh_r: None,
            mesh_g: None,
            mesh_b: None,
            mesh_a: None,
        }
    }
}

impl Default for FrameData {
    fn default() -> Self {
        FrameData {
            parent_frame_id: "world".to_string(),
            child_frame_id: "UNKNOWN".to_string(),
            transform: Transform {
                translation: Vector3 {
                    x: 0.0,
                    y: 0.0,
                    z: 0.0,
                },
                rotation: Quaternion {
                    x: 0.0,
                    y: 0.0,
                    z: 0.0,
                    w: 1.0,
                },
            },
            // json_path: "".to_string(),
            extra_data: ExtraData::default(),
        }
    }
}

pub async fn extra_tf_listener_callback(
    mut subscriber: impl Stream<Item = TFExtra> + Unpin,
    buffered_frames: &Arc<Mutex<HashMap<String, FrameData>>>,
    node_id: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        match subscriber.next().await {
            Some(message) => {
                let mut frames_local = buffered_frames.lock().unwrap().clone();
                message.data.iter().for_each(|t| {
                    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
                    let now = clock.get_now().unwrap();
                    let time_stamp = r2r::Clock::to_builtin_time(&now);

                    match serde_json::from_str(&t.extra) {
                        Ok::<ExtraData, _>(mut extras) => {
                            extras.time_stamp = Some(time_stamp);
                            frames_local.insert(
                                t.transform.child_frame_id.clone(),
                                FrameData {
                                    parent_frame_id: t.transform.header.frame_id.clone(),
                                    child_frame_id: t.transform.child_frame_id.clone(),
                                    transform: t.transform.transform.clone(),
                                    extra_data: extras,
                                },
                            );
                        }
                        Err(_) => todo!(),
                    }
                });
                *buffered_frames.lock().unwrap() = frames_local;
            }
            None => {
                r2r::log_error!(node_id, "Subscriber did not get the message?");
            }
        }
    }
}

pub async fn maintain_buffer(
    mut timer: r2r::Timer,
    buffered_frames: &Arc<Mutex<HashMap<String, FrameData>>>,
) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        let frames_local = buffered_frames.lock().unwrap().clone();
        let mut frames_local_reduced = frames_local.clone();
        let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
        let now = clock.get_now().unwrap();
        let current_time = r2r::Clock::to_builtin_time(&now);
        frames_local.iter().for_each(|(k, v)| {
            let stamp = v.clone().extra_data.time_stamp.unwrap(); // TODO: handle this nicer
            match v.extra_data.active {
                Some(true) | None => match current_time.sec > stamp.sec + FRAME_LIFETIME {
                    true => {
                        frames_local_reduced.remove(k);
                    }
                    false => (), // do nothing if the frame is fresh
                },
                Some(false) => (),
            }
        });
        *buffered_frames.lock().unwrap() = frames_local_reduced;
        timer.tick().await?;
    }
}