# /bin/bash

ros2 service call /path_planner/set_target gpss_planning_interfaces/srv/SetTarget "{'transporter_id': 'atr_16', 'target': 'target_16'}"
ros2 service call /path_planner/set_target gpss_planning_interfaces/srv/SetTarget "{'transporter_id': 'atr_12', 'target': 'target_12'}"
ros2 service call /path_planner/set_target gpss_planning_interfaces/srv/SetTarget "{'transporter_id': 'atr_10', 'target': 'target_10'}"