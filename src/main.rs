use futures::stream::{Stream, StreamExt};
use micro_sp::{
    self, simple_transition_planner, Action, Predicate, SPCommon, SPValue, SPVariable, State,
    ToSPValue, Transition,
};
use r2r::atr_object_msgs::msg::ObjectListStamped;
use r2r::builtin_interfaces::msg::Time;
use r2r::geometry_msgs::msg::{Point, TransformStamped};
use r2r::gpss_control_interfaces::msg::{ControllerState, Path, Waypoint};
use r2r::scene_manipulation_msgs::msg::TFExtra;
use r2r::scene_manipulation_msgs::srv::LookupTransform;
use r2r::std_msgs::msg::Bool;
use r2r::{self, QosProfile};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, Mutex};
use std::time::Duration;

mod math;
use math::*;

mod path_planner;
use path_planner::*;

mod parameters;
use parameters::*;

mod tf_extra_buffer;
use tf_extra_buffer::*;


#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct Blocked {
    pub blocked_connection: String,
    pub timestamp: Duration,
    pub transporter: Option<String>, // used if the blocking is only related to a specific transporter
}

impl Blocked {
    fn is_it_blocked(
        &self,
        wp: &str,
        transport_id: &str,
        now: Duration,
        time_before_blocked: Duration,
    ) -> bool {
        now > self.timestamp
            && now - self.timestamp > time_before_blocked
            && self
                .transporter
                .as_ref()
                .map(|t| t == transport_id)
                .unwrap_or(true)
            && &self.blocked_connection == wp
    }
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct WaypointsNConnections {
    pub waypoints: HashMap<String, Waypoint>, // key is waypoint id
    pub connections: HashMap<String, Vec<String>>, // key is waypoint id
    pub blocked: HashMap<String, Vec<Blocked>>, // key is waypoint id
}

impl WaypointsNConnections {
    fn get_unblocked_connections(
        &self,
        transport_id: &str,
        now: Duration,
        time_before_blocked: Duration,
    ) -> HashMap<String, Vec<String>> {
        self.connections
            .iter()
            .map(|(wp, next)| {
                let xs = next
                    .iter()
                    .filter(|nwp| {
                        self.blocked
                            .get(wp)
                            .map(|bs| {
                                let xx: Vec<&Blocked> = bs
                                    .iter()
                                    .filter(|b| {
                                        let x = b.is_it_blocked(
                                            nwp,
                                            transport_id,
                                            now,
                                            time_before_blocked,
                                        );
                                        if x {
                                            println!("IT IS BLOCKED {} - {}", wp, nwp);
                                        };
                                        x
                                    })
                                    .collect();
                                xx.is_empty()
                            })
                            .unwrap_or(true)
                    })
                    .cloned()
                    .collect();
                (wp.clone(), xs)
            })
            .collect()
    }

    fn delete_wp(&mut self, wp: &str) {
        self.waypoints.remove(wp);
        self.connections.remove(wp);
        self.blocked.remove(wp);
        let wp = wp.clone();
        for (_, xs) in self.connections.iter_mut() {
            xs.retain(|x| *x != wp)
        }
    }

    fn remove_all_wps(&mut self) {
        self.waypoints.clear();
        self.connections.clear();
    }

    fn add_wp(&mut self, wp: Waypoint, connections: Vec<String>) {
        let id = wp.id.clone();
        self.waypoints.insert(id.clone(), wp);
        self.connections.insert(id, connections);
    }

    fn update_connections(&mut self, wp: &str, connections: Vec<String>) {
        self.connections.insert(wp.to_string(), connections);
    }

    fn add_blocked(&mut self, wp: &str, blocked: Blocked) -> bool {
        let xs = self.blocked.entry(wp.to_string()).or_default();
        let res = xs
            .iter()
            .find(|x| x.blocked_connection == blocked.blocked_connection);
        if res.is_none() {
            xs.push(blocked);
            return true;
        }
        return false;
    }

    fn remove_blocked(&mut self, wp: &str, blocked: &str) {
        let bs = self.blocked.entry(wp.to_string()).or_default();
        bs.retain(|x| &x.blocked_connection != blocked);
        if bs.is_empty() {
            self.blocked.remove(wp);
        }
    }
}

#[derive(Debug, Clone, Default)]
struct SharedState {
    pub transporters: HashMap<String, SharedStateTransporter>, // key is atr id name
    pub waypoints: WaypointsNConnections,
    pub polygons: Vec<geo::Polygon>,
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
struct SharedStateTransporter {
    control_state: ControllerState,
    current_path: Option<Path>,
    run: bool,
    paths_to_run: Vec<Path>,
    force_new_path: bool,
}

impl SharedState {
    fn upd_control_state(&mut self, transport_id: &str, state: ControllerState) {
        let mut ts = self
            .transporters
            .entry(transport_id.to_string())
            .or_default();
        ts.control_state = state;
    }

    fn upd_current_path(&mut self, transport_id: &str, state: Option<Path>) {
        let mut ts = self
            .transporters
            .entry(transport_id.to_string())
            .or_default();
        ts.current_path = state;
    }
    fn upd_run(&mut self, transport_id: &str, state: bool) {
        let mut ts = self
            .transporters
            .entry(transport_id.to_string())
            .or_default();
        ts.run = state;
    }
    fn pop_paths_to_run(&mut self, transport_id: &str) {
        let mut ts = self
            .transporters
            .entry(transport_id.to_string())
            .or_default();
        let x = ts.paths_to_run.split_first();
        if let Some(x) = x {
            ts.paths_to_run = x.1.to_vec();
        };
    }
    fn add_paths_to_run(&mut self, transport_id: &str, path: Path) {
        let ts = self
            .transporters
            .entry(transport_id.to_string())
            .or_default();
        ts.paths_to_run.push(path);
    }
    fn clear_paths_to_run(&mut self, transport_id: &str) {
        let mut ts = self
            .transporters
            .entry(transport_id.to_string())
            .or_default();
        ts.paths_to_run = vec![];
    }
    fn upd_force_new_path(&mut self, transport_id: &str, state: bool) {
        let mut ts = self
            .transporters
            .entry(transport_id.to_string())
            .or_default();
        ts.force_new_path = state;
    }
}

async fn fetch_waypoints_tick(
    buffered_frames: &Arc<Mutex<HashMap<String, FrameData>>>,
    mut t: r2r::Timer,
    shared_state: Arc<Mutex<SharedState>>,
    tf_lookup_client: &r2r::Client<LookupTransform::Service>
) -> Result<(), Box<dyn std::error::Error>> {
    // let mut clock = r2r::Clock::create(r2r::ClockType::RosTime)?;
    loop {
        let frames_local = buffered_frames.lock().unwrap().clone();
        let _elapsed = t.tick().await?;
        // let now = clock.get_now()?;

        let mut filtered_correct_type: Vec<FrameData> = vec![];

        frames_local
            .iter()
            .for_each(|frame| match &frame.1.extra_data.frame_type {
                Some(has_type) => match has_type == "waypoint" {
                    true => filtered_correct_type.push(frame.1.clone()),
                    false => (),
                },
                None => (),
            });

        let mut nodes: Vec<(Waypoint, Vec<String>)> = vec!();
        for filtered in filtered_correct_type {
            match lookup_tf("world", &filtered.child_frame_id, tf_lookup_client).await {
                Some(found) => {
                    let q = nalgebra::Quaternion::from_vector(nalgebra::Vector4::new(
                        found.transform.rotation.x,
                        found.transform.rotation.y,
                        found.transform.rotation.z,
                        found.transform.rotation.w,
                    ));
                    let (_, _, direction) = nalgebra::UnitQuaternion::from_quaternion(q).euler_angles();
                    let zone = filtered.extra_data.zone.unwrap_or_default();
                    let next = filtered.extra_data.next.unwrap_or_default().iter().map(|x| x.clone()).collect();
                    nodes.push(
                        (
                            Waypoint {
                                id: found.child_frame_id,
                                point: Point {
                                    x: found.transform.translation.x,
                                    y: found.transform.translation.y,
                                    z: 0.0,
                                },
                                direction,
                                zone,
                            },
                            next
                        )
                    )
                },
                None => ()
                
            }
        }

        let mut ss = shared_state.lock().unwrap();
        ss.waypoints.remove_all_wps();
        for n in nodes {
            ss.waypoints.add_wp(n.0, n.1)
        }
    }
}

async fn tick(
    transport_id: String,
    path_service: r2r::Client<r2r::gpss_control_interfaces::srv::SetPath::Service>,
    run_pub: r2r::Publisher<Bool>,
    mut t: r2r::Timer,
    shared_state: Arc<Mutex<SharedState>>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime)?;
    let now = clock.get_now()?;
    let _tick_time = r2r::Clock::to_builtin_time(&now);

    loop {
        let _elapsed = t.tick().await?;

        let state = shared_state
            .lock()
            .unwrap()
            .transporters
            .get(&transport_id)
            .cloned();
        if state.is_none() {
            continue;
        }
        let state = state.unwrap();
        let path = state.current_path;
        let run = state.run;
        let controller = state.control_state;
        let paths_to_run = state.paths_to_run;

        if !controller.running && !run && !paths_to_run.is_empty() {
            println!("[{}] sending...", transport_id);
            let set_path = paths_to_run[0].clone();
            let r = r2r::gpss_control_interfaces::srv::SetPath::Request {
                path: set_path.clone(),
            };
            let result = path_service.request(&r)?.await?;

            if result.accepted {
                let mut state = shared_state.lock().unwrap();
                state.upd_current_path(&transport_id, Some(set_path));
                state.pop_paths_to_run(&transport_id);
                state.upd_run(&transport_id, true);
                run_pub.publish(&Bool { data: true })?;
            } else {
                println!("[{}] The path was not accepted!!", transport_id)
            }
        } else if path.is_some() && controller.running && controller.path_completed {
            println!("[{}] We are done!", transport_id);
            {
                if let Some(ref mut ss) = shared_state
                    .lock()
                    .unwrap()
                    .transporters
                    .get_mut(&transport_id)
                {
                    ss.run = false;
                    ss.current_path = None;
                }
            }

            run_pub.publish(&Bool { data: false })?;
        } else if !run && controller.running {
            run_pub.publish(&Bool { data: false })?;
        }
    }
}

async fn check_if_blocked_tick(
    shared_state: Arc<Mutex<SharedState>>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut t = tokio::time::interval(Duration::from_secs_f64(1.0));
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime)?;
    loop {
        t.tick().await;

        let now = clock.get_now()?;

        let (mut wps, polygons) = {
            let ss = shared_state.lock().unwrap();
            (ss.waypoints.clone(), ss.polygons.clone())
        };

        // let wnc = &ss.waypoints;
        // check all obstacles against all paths.
        // put this in a spawn blocking if it takes to long,
        // or perform it less frequently.
        use geo::Intersects;
        for (from, targets) in wps.connections.clone().iter() {
            // for each (from, to) compute polygon and check collisions
            if !wps.waypoints.contains_key(from) {
                continue;
            }
            let from_wp = wps.waypoints[from].clone();
            for to in targets {
                if !wps.waypoints.contains_key(to) {
                    continue;
                }
                let to_wp = &wps.waypoints[to].clone();
                let poly = compute_poly_between_wps(&from_wp, &to_wp);
                let mut xs = vec![];
                if polygons.iter().any(|p| poly.intersects(p)) {
                    xs.push(Blocked {
                        blocked_connection: to.to_string(),
                        timestamp: now,
                        transporter: None,
                    });
                }
                if xs.is_empty() && wps.blocked.contains_key(from) {
                    // if wps.blocked[from].iter().find(|x| &x.blocked_connection == to).is_some() {
                    //     println!("REMOVING BLOCKED {} - {}", from, to);
                    // }
                    wps.remove_blocked(&from, &to);
                } else if !xs.is_empty() {
                    if wps.add_blocked(&from, xs[0].clone()) {
                        // println!("ADDING BLOCKED {} - {}", from, to);
                    }
                }
            }
        }

        for (wp, bs) in wps.blocked.iter() {
            for b in bs.iter() {
                if b.timestamp < now && now - b.timestamp > Duration::from_secs_f64(30.0) {
                    println!("{} to {} is blocked", wp, b.blocked_connection);
                }
            }
        }

        shared_state.lock().unwrap().waypoints = wps;
    }
}

async fn update_controller_state(
    transport_id: String,
    mut sub: impl Stream<Item = ControllerState> + Unpin,
    shared_state: Arc<Mutex<SharedState>>,
) {
    loop {
        if let Some(msg) = sub.next().await {
            //println!("I GOT : {msg:?}");
            let mut state = shared_state.lock().unwrap();
            state.upd_control_state(&transport_id, msg);
        }
    }
}

fn handle_path_planning_request(
    req: &r2r::gpss_planning_interfaces::srv::SetTarget::Request,
    shared_state: Arc<Mutex<SharedState>>,
    now: Duration,
    time_before_blocked: Duration,
) -> Option<bool> {
    // todo...
    let (wps, transporters) = {
        let ss = shared_state.lock().unwrap();
        (ss.waypoints.clone(), ss.transporters.clone())
    };

    let atr_id = req.transporter_id.clone();
    if !transporters.contains_key(&atr_id) {
        println!("No transporters with name: {}", atr_id);
        return None;
    }
    let target = req.target.clone();
    if !wps.waypoints.contains_key(&target) {
        println!("No target frame with the name: {}", target);
        return None;
    }

    println!("New request for {}, target: {}", atr_id, target);

    let current_point = transporters
        .get(&atr_id)
        .map(|state| state.control_state.pose.position.clone())?;

    println!("[{}] Current position {:?}", atr_id, current_point);
    let closest_wp = find_closest_waypoint(current_point, &wps, 5.0)?;

    println!("[{}] Closest wp {:?}", atr_id, closest_wp);
    let target = wps.waypoints.get(&target).unwrap();

    let all_non_blocked_connections =
        wps.get_unblocked_connections(&atr_id, now, time_before_blocked);
    let mut path = path_planner::compute_path(
        &closest_wp,
        target,
        &wps.waypoints,
        &all_non_blocked_connections,
    );

    if path.is_none() {
        path = path_planner::compute_path(&closest_wp, target, &wps.waypoints, &wps.connections);
    };
    let path = path?;

    println!("[{}] New path computed:", atr_id);
    for x in &path.waypoints {
        println!("[{}] {:?}", atr_id, x);
    }

    let mut ss = shared_state.lock().unwrap();
    ss.add_paths_to_run(&atr_id, path);
    ss.upd_run(&atr_id, false);

    Some(true)
}

async fn update_objects(
    mut sub: impl Stream<Item = ObjectListStamped> + Unpin,
    shared_state: Arc<Mutex<SharedState>>,
) {
    loop {
        if let Some(msg) = sub.next().await {
            let mut polygons = vec![];
            for objects in &msg.objects {
                if objects.polygon.points.len() > 2
                // Polygon, add to our list
                {
                    let p = geometry_polygon_to_geo_polygon(&objects.polygon);
                    polygons.push(p);
                }
            }
            shared_state.lock().unwrap().polygons = polygons;
        }
    }
}

// TODO: double check the computed polygon.
fn compute_poly_between_wps(wp1: &Waypoint, wp2: &Waypoint) -> geo::Polygon {
    // compute angle between wps
    let x = wp2.point.x - wp1.point.x;
    let y = wp2.point.y - wp1.point.y;
    let angle = atan2(y, x);

    // create a 1.0m wide polygon between the waypoints
    let width = 1.0;

    let (x1, y1) = rotate_point(-width / 2.0, 0.0, angle);
    let (x2, y2) = rotate_point(width / 2.0, 0.0, angle);

    let waypoint_poly = vec![
        (wp1.point.x + x1, wp1.point.y + y1),
        (wp2.point.x + x1, wp2.point.y + y1),
        (wp2.point.x + x2, wp2.point.y + y2),
        (wp1.point.x + x2, wp1.point.y + y2),
    ];

    geo::Polygon::new(geo::LineString::from(waypoint_poly), vec![])
}

pub struct TransitionInfo {
    pub agv_name: String,
    pub from: String,
    pub to: String,
}

async fn tick_hl_planner(
    run_pub: HashMap<String, r2r::Publisher<Bool>>,
    state: Arc<Mutex<SharedState>>,
) {
    let mut t = tokio::time::interval(Duration::from_secs_f64(1.0));
    loop {
        {
            let mut agv_hl_paths_pairs = vec![];

            let state_local = state.lock().unwrap();

            for t1 in &state_local.transporters {
                let mut new_path = vec![];
                if t1.1.control_state.path.len() != 1 {
                    for wp in &t1.1.control_state.path {
                        if wp == "none" {
                            // have to handle
                            continue;
                        }
                        for t2 in &state_local.transporters {
                            if t2.1.control_state.path.contains(&wp) && t1.0 != t2.0 {
                                new_path.push(wp.clone())
                            }
                        }
                    }
                    let original_last = t1.1.control_state.path.last().unwrap();
                    if !new_path.is_empty() && new_path.last().unwrap() != original_last {
                        new_path.push(original_last.clone())
                    }
                    if !new_path.is_empty() {
                        agv_hl_paths_pairs.push((t1.0.clone(), new_path))
                    }
                }
            }
            for ahpp in &agv_hl_paths_pairs {
                println!("{:?}", ahpp)
            }

            let agv_vars: Vec<SPVariable> = agv_hl_paths_pairs
                .iter()
                .map(|x| {
                    let domain = x.1.iter().map(|y| y.as_str().to_spval()).collect();
                    micro_sp::SPVariable::new(&x.0, &micro_sp::SPValueType::String, &domain)
                })
                .collect();

            let mut transition_infos: HashMap<String, TransitionInfo> = HashMap::new();
            let transitions: Vec<Vec<Transition>> = agv_vars
                .iter()
                .map(|v| {
                    let others: Vec<SPVariable> = agv_vars
                        .iter()
                        .filter(|x| x.name != v.name)
                        .map(|x| x.to_owned())
                        .collect();
                    v.domain
                        .windows(2)
                        .map(|x| {
                            // myself am in curr
                            let curr = &x[0];
                            let next = &x[1];

                            let guard_part_1 = Predicate::EQ(
                                SPCommon::SPVariable(v.clone()),
                                SPCommon::SPValue(curr.clone()),
                            );

                            // someone else can't be at my next position
                            let guard_part_2 = Predicate::AND(
                                others
                                    .iter()
                                    .map(|o| {
                                        Predicate::NOT(Box::new(Predicate::EQ(
                                            SPCommon::SPVariable(o.clone()),
                                            SPCommon::SPValue(next.clone()),
                                        )))
                                    })
                                    .collect(),
                            );

                            let guard = Predicate::AND(vec![guard_part_1, guard_part_2]);
                            let action = Action::new(v.clone(), SPCommon::SPValue(next.clone()));
                            let transition_name = &format!("{}_from_{}_to_{}", v.name, curr, next);
                            let transition = Transition::new(transition_name, guard, vec![action]);
                            transition_infos.insert(
                                transition_name.to_owned(),
                                TransitionInfo {
                                    agv_name: v.name.to_owned(),
                                    from: curr.to_string(),
                                    to: next.to_string(),
                                },
                            );
                            transition
                        })
                        .collect()
                })
                .collect();

            let transitions: Vec<Transition> = transitions.into_iter().flatten().collect();

            let init = State {
                state: agv_vars
                    .iter()
                    .map(|var| (var.clone(), var.domain[0].clone()))
                    .collect::<HashMap<SPVariable, SPValue>>(),
            };
            let goal = Predicate::AND(
                agv_vars
                    .iter()
                    .map(|var| {
                        Predicate::EQ(
                            SPCommon::SPVariable(var.clone()),
                            SPCommon::SPValue(var.domain.last().unwrap().clone()),
                        )
                    })
                    .collect(),
            );
            let result = simple_transition_planner(init, goal, transitions.clone(), 30);
            println!("plan: {:?}", result.plan);

            for t1 in &state_local.transporters {
                let mut can_go = true;
                if t1.1.control_state.path.len() != 1 {
                    let next_pos = t1.1.control_state.path[1].clone();
                    // find the first plan step with next pos as next
                    // if atr id != us, we need to stop
                    println!("agv id = {} going to {}", t1.0, next_pos);
                    for p in &result.plan {
                        match transition_infos.get(p) {
                            Some(info) => {
                                if info.from != next_pos {
                                    continue;
                                } else {
                                    if &info.agv_name != t1.0 {
                                        println!("[{}] We need to wait for {}", t1.0, p);
                                        can_go = false;
                                        break;
                                    } else {
                                        println!("[{}] I get to go first {}", t1.0, p);
                                        break;
                                    }
                                }
                            }
                            None => continue,
                        }
                    }
                }
                let publisher = run_pub.get(t1.0).expect("no such atr run pub");
                if !t1.1.control_state.path_completed {
                    publisher.publish(&Bool { data: can_go }).expect("asdf")
                }
            }
        }

        t.tick().await;
    }
}

async fn lookup_tf(
    parent_frame_id: &str,
    child_frame_id: &str,
    tf_lookup_client: &r2r::Client<LookupTransform::Service>,
) -> Option<TransformStamped> {
    let request = LookupTransform::Request {
        parent_frame_id: parent_frame_id.to_string(),
        child_frame_id: child_frame_id.to_string(),
    };

    let response = tf_lookup_client
        .request(&request)
        .expect("Could not send tf Lookup request.")
        .await
        .expect("Cancelled.");

    r2r::log_info!(
        NODE_ID,
        "Request to lookup parent '{}' to child '{}' sent.",
        parent_frame_id,
        child_frame_id
    );

    match response.success {
        true => Some(response.transform),
        false => {
            r2r::log_error!(
                NODE_ID,
                "Couldn't lookup tf for parent '{}' and child '{}'.",
                parent_frame_id,
                child_frame_id
            );
            None
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let ctx = r2r::Context::create()?;
    let mut node = r2r::Node::create(ctx, "path_planner", "")?;

    // read parameters
    let parameters = init_parameters(node.params.lock().unwrap().clone());
    let atr_ids = parameters.transporters.clone();
    let shared_state = Arc::new(Mutex::new(SharedState::default()));

    let mut atr_run_pubs = HashMap::new();

    println!("Setting up for atr ids: {:?}", atr_ids);
    for id in atr_ids {
        let topic = format!("/{id}/controller_state");
        let control_state_sub =
            node.subscribe::<ControllerState>(&topic, r2r::QosProfile::default())?;
        let shared_state_task = shared_state.clone();

        let id_t = id.to_string();
        tokio::task::spawn(async move {
            update_controller_state(id_t, control_state_sub, shared_state_task).await
        });

        let topic = format!("/{id}/set_path");
        let path_service =
            node.create_client::<r2r::gpss_control_interfaces::srv::SetPath::Service>(&topic)?;
        let topic = format!("/{id}/run");
        let run_pub = node.create_publisher::<Bool>(&topic, r2r::QosProfile::default())?;
        let run_pub2 = node.create_publisher::<Bool>(&topic, r2r::QosProfile::default())?;
        atr_run_pubs.insert(id.clone(), run_pub2);
        let timer = node.create_wall_timer(Duration::from_secs_f64(1.0))?;
        let shared_state_task = shared_state.clone();
        tokio::task::spawn(async move {
            tick(
                id.to_string(),
                path_service,
                run_pub,
                timer,
                shared_state_task,
            )
            .await
            .expect("fail");
        });
    }

    let tf_lookup_client = node.create_client::<LookupTransform::Service>("/lookup_transform")?;
    let waiting_for_tf_lookup_server = node.is_available(&tf_lookup_client)?;

     // a buffer of frames that exist on the tf_extra topic
     let buffered_frames = Arc::new(Mutex::new(HashMap::<String, FrameData>::new()));

    // fetch_waypoints_tick
    // let get_frames_service = node
    //     .create_client::<r2r::scene_manipulation_msgs::srv::GetAllExtra::Service>(
    //         "/get_all_extras",
    //     )?;
    let timer = node.create_wall_timer(Duration::from_secs_f64(1.0))?;
    let shared_state_task = shared_state.clone();
    // let waiting = node.is_available(&get_frames_service).unwrap();
    let buffered_frames_clone = buffered_frames.clone();
    tokio::task::spawn(async move {
        // waiting.await.unwrap();
        fetch_waypoints_tick(&buffered_frames_clone, timer, shared_state_task, &tf_lookup_client)
            .await
            .expect("fail");
    });

   

    let extra_tf_listener =
        node.subscribe::<TFExtra>("tf_extra", QosProfile::best_effort(QosProfile::default()))?;
    let buffered_frames_clone = buffered_frames.clone();
    tokio::task::spawn(async move {
        match extra_tf_listener_callback(extra_tf_listener, &buffered_frames_clone.clone(), NODE_ID)
            .await
        {
            Ok(()) => (),
            Err(e) => r2r::log_error!(NODE_ID, "Extra tf listener failed with: '{}'.", e),
        };
    });

    // spawn a tokio task to maintain the tf extra buffer by removing stale frames
    let buffer_maintain_timer =
        node.create_wall_timer(std::time::Duration::from_millis(BUFFER_MAINTAIN_RATE))?;
    let buffered_frames_clone = buffered_frames.clone();
    tokio::task::spawn(async move {
        match maintain_buffer(buffer_maintain_timer, &buffered_frames_clone).await {
            Ok(()) => (),
            Err(e) => r2r::log_error!(NODE_ID, "Buffer maintainer failed with: '{}'.", e),
        };
    });

    // for hl planner
    // let timer = node.create_wall_timer(Duration::from_secs_f64(1.0))?;
    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move {
        tick_hl_planner(atr_run_pubs, shared_state_task).await;
    });

    // check if some connections are blocked
    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move {
        check_if_blocked_tick(shared_state_task)
            .await
            .expect("fail");
    });

    let object_sub = node.subscribe::<ObjectListStamped>(
        "/area_0/global_objects_fused",
        r2r::QosProfile::default(),
    )?;

    let shared_state_task = shared_state.clone();
    tokio::task::spawn(async move { update_objects(object_sub, shared_state_task).await });

    // Set path service call
    use r2r::gpss_planning_interfaces::srv::SetTarget;
    let topic = "/path_planner/set_target";
    let shared_state_task = shared_state.clone();
    let mut service = node.create_service::<SetTarget::Service>(&topic)?;
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime)?;
    let params = parameters.clone();
    tokio::task::spawn(async move {
        loop {
            match service.next().await {
                Some(req) => {
                    println!(
                        "GOT REQUEST. ATR {}, TARGET: {}",
                        req.message.transporter_id, req.message.target
                    );

                    let now = clock.get_now().unwrap();

                    let resp = if let Some(accepted) = handle_path_planning_request(
                        &req.message,
                        shared_state_task.clone(),
                        now,
                        Duration::from_secs_f64(parameters.seconds_before_blocked),
                    ) {
                        SetTarget::Response { accepted }
                    } else {
                        println!("Some error occured");
                        SetTarget::Response { accepted: false }
                    };

                    req.respond(resp).expect("could not send service response");
                }
                None => break,
            }
        }
    });

    

    let handle = tokio::task::spawn_blocking(move || loop {
        node.spin_once(std::time::Duration::from_millis(100));
    });

    // await spin task (will never finish)
    handle.await?;

    Ok(())
}

fn geometry_polygon_to_geo_polygon(poly: &r2r::geometry_msgs::msg::Polygon) -> geo::Polygon {
    // From geo docs: If a LineString’s first and last Coordinate have different values, a new Coordinate will be appended to the LineString with a value equal to the first Coordinate.
    // So we don't need to add more points.
    let points: Vec<geo::Coordinate<f64>> = poly
        .points
        .iter()
        .map(|p| {
            geo::Coordinate::<f64> {
                x: p.x.into(),
                y: p.y.into(),
            } // we ignore z
        })
        .collect();
    geo::Polygon::new(geo::LineString::from(points), vec![])
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    #[test]
    fn test_bfs_planning() {
        // let wps = create_planning_problem();
        // let nodes = load_wps();

        // let start = nodes.get("grid_1_1").unwrap();
        // let goal = nodes.get("grid_1_4").unwrap();

        // let p = path_planner::compute_path(start, goal, wps ).unwrap();

        // for x in p.waypoints {
        //     println!("{:?}", x);
        // }
    }
}
