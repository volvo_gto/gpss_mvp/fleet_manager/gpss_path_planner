use r2r::geometry_msgs::msg::Point;
use r2r::gpss_control_interfaces::msg::{Path, Waypoint};
use std::collections::HashMap;
use std::hash::Hash;

use crate::WaypointsNConnections;

use super::math::*;

#[derive(Debug, Clone)]
pub struct Pimped {
    wp: Waypoint,
    path: Vec<Waypoint>,
    g: f64,
    h: f64,
}


pub fn compute_path(
    current_waypoint: &Waypoint,
    goal: &Waypoint,
    waypoints: &HashMap<String, Waypoint>,
    connections: &HashMap<String, Vec<String>>
) -> Option<Path> {
    let mut stack: Vec<Pimped> = Vec::new();
    let goal = goal.clone();
    let mut visited: HashMap<String, f64> = HashMap::new();

    stack.push(Pimped {
        wp: current_waypoint.clone(),
        path: vec![current_waypoint.clone()],
        g: 0.0,
        h: 0.0,
    });

    while !stack.is_empty() {

        let mut c_i = 0;
        let mut c = -1.0;

        for (i, x) in stack.iter().enumerate() {
            if c == -1.0 || c > (x.g + x.h) {
                c_i = i;
                c = x.g + x.h
            };
        }

        let x = stack.swap_remove(c_i);

        let current_cost = visited.entry(x.wp.id.clone()).or_insert(x.g + x.h);

        if *current_cost < x.g + x.h {
            continue;
        }
        *current_cost = x.g + x.h;

        if x.wp == goal {
            return Some(Path {
                waypoints: x.path.clone(),
            });
        }

        if let Some(next_wps) = connections.get(&x.wp.id){
            for wp_id in next_wps {
                if let Some(wp) = waypoints.get(wp_id) {
                    let mut upd = x.path.clone();
                    upd.push(wp.clone());
        
                    let vec_x = x.wp.point.x - wp.point.x;
                    let vec_y = x.wp.point.y - wp.point.y;
                    let cost = sqrt(vec_x * vec_x + vec_y * vec_y);
                    let goal_x = goal.point.x - wp.point.x;
                    let goal_y = goal.point.y - wp.point.y;
                    let cost_h = sqrt(goal_x * goal_x + goal_y * goal_y);
        
                    stack.push(Pimped {
                        wp: wp.clone(),
                        path: upd,
                        g: x.g + cost,
                        h: cost_h,
                    });

                }
        }
        }
    }
    return None;
}

pub fn find_closest_waypoint(
    current_position: Point,
    waypoints: &WaypointsNConnections,
    max_distance: f64,
) -> Option<Waypoint> {
    let mut result = None;
    let mut best = -1.0;
    for wp in waypoints.waypoints.values() {
        let d = distance(&current_position, &wp.point);
        if best < 0.0 || (d < max_distance && best > d) {
            result = Some(wp.clone());
            best = d;
        }
    }
    return result;
}

pub fn distance(p1: &Point, p2: &Point) -> f64 {
    let x = p2.x - p1.x;
    let y = p2.y - p1.y;
    return sqrt(x * x + y * y);
}
