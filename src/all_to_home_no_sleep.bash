# /bin/bash

ros2 service call /path_planner/set_target gpss_planning_interfaces/srv/SetTarget "{'transporter_id': 'atr_10', 'target': 'home_10'}"
ros2 service call /path_planner/set_target gpss_planning_interfaces/srv/SetTarget "{'transporter_id': 'atr_12', 'target': 'home_12'}"
ros2 service call /path_planner/set_target gpss_planning_interfaces/srv/SetTarget "{'transporter_id': 'atr_16', 'target': 'home_16'}"