#![allow(non_upper_case_globals, dead_code, unused)]

use std::f64;

// math helpers to avoid ugly postfix expressions
pub const sqrt: fn(f64) -> f64 = f64::sqrt;
pub const abs: fn(f64) -> f64 = f64::abs;
pub const cos: fn(f64) -> f64 = f64::cos;
pub const sin: fn(f64) -> f64 = f64::sin;
pub const atan2: fn(f64, f64) -> f64 = f64::atan2;
pub const min: fn(f64, f64) -> f64 = f64::min;
// pub const max: fn(f64, f64) -> f64 = f64::max;
pub const sign: fn(f64) -> f64 = f64::signum;

pub fn rotate_point(x: f64, y: f64, angle: f64) -> (f64, f64) {
    let ang_c = cos(angle);
    let ang_s = sin(angle);
    let x_prime = x * ang_c - y * ang_s;
    let y_prime = x * ang_s + y * ang_c;

    (x_prime, y_prime)
}
